void waiter(int microseconds) {
  while (microseconds <= 1000) {
    delayMicroseconds(1000);
    microseconds -= 1000;
  }
  delayMicroseconds(microseconds);
}
bool isOn(int pin) {
  double voltage = 0;
  for (int i = 0; i < SAMPLES; i++, waiter(SAMPLE_DELAY)) {
    voltage += (double)analogRead(pin) * (5.0 / 1024.0);
  }
  voltage = voltage / (double)SAMPLES;
  if (DEBUG)
    Serial.println(String(pin) + ": " + String(voltage));
  if (voltage > 4.0)
    return false;
  return true;
}


/*
   TODO: Finish this function first then start other things!!
   See you on monday. bye. :)
   Welcome back bro... finished this function :)

*/
void applianceControl(int pin, bool newState) {
  if (DEBUG)
    Serial.println("Controlling: " + String(pin) + ":" + String(newState) + ".");

  //Masked code to protect patent secret!
  // do not touch next two lines
  bool currentCalculatedApplianceState =
    (!digitalRead(loadRelayPins[pin]) & applianceStates[pin]) |
    (digitalRead(loadRelayPins[pin]) & digitalRead(mobileRelayPins[pin]));

  bool loadRelayNewState =
    //    (currentCalculatedApplianceState & applianceStates[pin]) |
    (~currentCalculatedApplianceState & newState) |
    (~applianceStates[pin] & newState) |
    (applianceStates[pin] & ~newState);
  bool mobileRelayNewState =
    (!currentCalculatedApplianceState & newState ) |
    (!applianceStates[pin] & newState);

  // hali 2
  loadRelayNewState =
    newState | applianceStates[pin];
  mobileRelayNewState =
    newState;


  //  bool loadRelayNewState = applianceStates[pin] ^ newState;
  //  bool mobileRelayNewState = (~applianceStates[pin]) & newState;
  if (DEBUG)
    Serial.print("currentCalculatedApplianceState: " + String(currentCalculatedApplianceState) + "loadRelayNewState: " + String(loadRelayNewState) + " mobileRelayNewState: " + String(mobileRelayNewState));
  //  bool loadRelayNewState=!newState;/*digitalRead(loadRelayPins[pin])*/
  //  bool mobileRelayNewSTate=

  digitalWrite(loadRelayPins[pin], loadRelayNewState);
  digitalWrite(mobileRelayPins[pin], mobileRelayNewState);

}


void populateApplianceStates() {
  bool temp;
  for (int i = 0; i < 1; i++) {
    temp = isOn(inputPins[i]);

    // if stored applianceState is different than newState, it is changed!
    isChangedInternal[i] = (applianceStates[i] != temp);

    //first if changed, then update relay states ( control appliance for new state)
    if (isChangedInternal[i])
      applianceControl(i, temp);
    if (DEBUG)
      Serial.println("Changed internal?: " + String(isChangedInternal[i]) + ",sensed state: " + String(temp));
    //set new appliance state
    applianceStates[i] = temp;
  }
  if (DEBUG)
    Serial.println();
}
bool isChanged(int pin) {
  if (isChangedInternal[pin]) {
    isChangedInternal[pin] = false;
    return true;
  }
  return false;
}
