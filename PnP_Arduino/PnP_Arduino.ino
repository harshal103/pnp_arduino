#include "Configuration.h"
#include "util.h"
#include "ESPCommunication.h"

void setup() {
  for(int i=0;i<4;i++){
    pinMode(loadRelayPins[i],OUTPUT);
    pinMode(mobileRelayPins[i],OUTPUT);
  }
  Serial.begin(4800);
  while (!Serial);
  delay(2000);
  //  Serial.println("Welcome to arduino!");
  populateApplianceStates();
  reportToESP();
}

void loop() {
  /*
     update appliance states.
     if changed, report to esp
  */
  populateApplianceStates();
  reportToESP();
  if (Serial.available()) {
    
    String payLoadFromESP = Serial.readString();
    DynamicJsonBuffer buffer(JSON_PARSER_SIZE_SINGLE_PIN);
    JsonObject& ittyBittyObject = buffer.parseObject(payLoadFromESP);
    if (ittyBittyObject["request"].as<String>() == "true") {
      reportToESP(true);
      return;
    }
    
    if (DEBUG) {
      Serial.print("ESP says: ");
      Serial.print(payLoadFromESP);
//      ittyBittyObject.printTo(Serial);
      Serial.println("\r");
    }
    if(ittyBittyObject.success()){
      // If parsing is successful then only control appliance
      applianceControl(ittyBittyObject["pin_no"].as<int>(), ittyBittyObject["pin_state"].as<int>());
      
    }
  }
  if (DEBUG){
    Serial.print("LoadRelay0: ");
    Serial.print(digitalRead(loadRelayPins[0]));
    Serial.print(" MobileRelay0: ");
    Serial.print(digitalRead(mobileRelayPins[0]));
    Serial.print(" SwitchState0: ");
    Serial.print(applianceStates[0]);
    
    Serial.println();
  }
  if (DEBUG)
    delay(1000);
  
}
