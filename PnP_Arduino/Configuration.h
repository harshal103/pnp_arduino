#define SAMPLES 5
#define SAMPLE_DELAY 1100
#define DEBUG false
/*
   Array which represents on which pin does
   the input for appliance state is sensed.

   This array stores pin numbers on
   which input samples are collected
*/
int inputPins[] = {A4, A1, A2, A3};

/*
   This array stores pin numbers for Load-Relay
   for i'th appliance
*/
int loadRelayPins[] = {10, 8, 6, 12};

/*
   This array stores pin numbers for Mobile Relay
   for i'th appliance
*/
int mobileRelayPins[] = {11, 9, 7, 13};

bool applianceStates[] = {false, false, false, false};

bool isChangedInternal[] = {false, false, false, false};

#define JSON_PARSER_SIZE JSON_ARRAY_SIZE(4) + JSON_OBJECT_SIZE(1) + 4*JSON_OBJECT_SIZE(3)+125
#define JSON_PARSER_SIZE_SINGLE_PIN JSON_OBJECT_SIZE(3)+29
