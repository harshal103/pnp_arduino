#include "ArduinoJson.h"


void reportToESP(bool all=false) {
  //create json object
  DynamicJsonBuffer pinBuffer(JSON_PARSER_SIZE_SINGLE_PIN);
  JsonObject& pinObject=pinBuffer.createObject();

  //set notify to always false
//  pinObject["notify"]=false;
  // we have 4 pins
  for(int i=0;i<4;i++)
  {
    //report only if appliance state is changed or 'all' variable is true.
    if(all || isChanged(i)){
      
      pinObject["pin"]=i;
//      pinObject["state"]=(int)applianceStates[i];
      pinObject["state"] = (int)(!digitalRead(loadRelayPins[i]) & applianceStates[i]) |
    (digitalRead(loadRelayPins[i]) & digitalRead(mobileRelayPins[i]));
      pinObject.printTo(Serial);
      Serial.println("\r");
      Serial.flush();
    }
  }
  /*
  DynamicJsonBuffer rootBuffer(4*JSON_OBJECT_SIZE(2)+JSON_OBJECT_SIZE(4));
  DynamicJsonBuffer pinBuffer(JSON_OBJECT_SIZE(2));
  JsonObject& root=rootBuffer.createObject();
  JsonObject& pin=pinBuffer.createObject();
  pin["changed"]=false;
  for(int i=0;i<4;i++) {
    pin["status"]=applianceStates[i];
    root["pin"+String(i)]=pin;
  }
  root.printTo(Serial);
  */
  /*
  root["changed"]=false;
  root["pin1"]=applianceStates[0];
  root["pin2"]=applianceStates[1];
  root["pin3"]=applianceStates[2];
  root["pin4"]=applianceStates[3];
  root.printTo(Serial);
  */
}

/*
JsonObject& fetchFromESP() {
  DynamicJsonBuffer buffer(4*JSON_OBJECT_SIZE(2)+JSON_OBJECT_SIZE(4));
  String payLoadFromESP=Serial.readString();
  JsonObject& root=buffer.parseObject(payLoadFromESP);
  return root;
}
*/
